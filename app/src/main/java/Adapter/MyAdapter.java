package Adapter;

import android.app.LauncherActivity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.hp.recyclerview.MainActivity;
import com.example.hp.recyclerview.R;
import com.example.hp.recyclerview.infoAct;

import java.util.List;

import Model.ListItem;

/**
 * Created by hp on 08-02-2018.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private Context context;
    private List<ListItem> listItems;
    public MyAdapter(Context context, List listItem){
        this.context = context;
        this.listItems = listItem;

    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
        ListItem Items=listItems.get(position);
        holder.Title.setText(Items.getTitle());
        holder.info.setText(Items.getinfo());

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView Title;
        public TextView info;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            Title=(TextView)itemView.findViewById(R.id.title);
            info=(TextView)itemView.findViewById(R.id.info);
        }

        @Override
        public void onClick(View v) {
            int position= getAdapterPosition();
            ListItem item=listItems.get(position);
            Intent intent=new Intent(context,infoAct.class);
            intent.putExtra("title",item.getTitle());
            intent.putExtra("info",item.getinfo());
            context.startActivity(intent);
        }
    }
}
