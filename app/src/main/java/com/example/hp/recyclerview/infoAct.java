package com.example.hp.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class infoAct extends AppCompatActivity {
    private TextView main,sub;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        main=(TextView)findViewById(R.id.textView);
        sub=(TextView)findViewById(R.id.textView2);
        extras=getIntent().getExtras();
        if(extras!=null){
            main.setText(extras.getString("title"));
            sub.setText(extras.getString("info"));
        }

    }
}
