package Model;

/**
 * Created by hp on 08-02-2018.
 */

public class ListItem {
    private String Title;
    private String info;

    public  ListItem(String Title, String info){
        this.Title=Title;
        this.info=info;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
       this.Title =Title;
    }

    public String getinfo() {
        return info;
    }

    public void setinfo(String info) {
        this.info = info;
    }
}
